<?php
$app->get('router')->get('/Home', function() use($app) {
    $app->view('Home');
 });

 $app->get('router')->get('/About', function() use($app) {
    $app->view('About');
 });
 
 $app->get('router')->get('/Services', function() use($app) {
    $app->view('Services');
 });

 $app->get('router')->get('/Projects', function() use($app) {
    $app->view('Projects');
 });

 $app->get('router')->get('/ContactUs', function() use($app) {
    $app->view('ContactUs');
 });
 

 $app->get('router')->get('/{slug}', function($slug) use($app) {
   echo "Param: $slug";

 });
 
 $app->get('router')->get('/api', function() use($app, $config) {
     $app->json(["api_key" => $config['api']['key']]);
 });
 
 $app->get('router')->post('/api', function() use($app) {
     $app->json($app->request->getJson(), 201);
 });
?>