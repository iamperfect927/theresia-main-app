<?php 
$navbar = array(
  "Home" => "Home",
  "About" => "About",
  "Services" => "Services",
  "Projects" => "Projects",
  "ContactUs" => "Contact Us",
);

function navList($navbar){
  foreach($navbar as $a => $b){
    echo '<li class="nav-item"><a class="nav-link" href="'.$a.'">'.$b.'</a></li>';
  }
}
?>