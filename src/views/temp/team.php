<div class="container">
              <div class="text-center">
                <h2>Meet Our Team</h2>
              </div>
              
              <div class="row g-4 mt-3">
                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_1.jpg" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>
              

                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_2.jpg" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_3.jpg" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_4.jpg" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row g-4 mb-5 mt-3">
                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_5.jpg" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>
                
                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_6.jpg" height="130px" width="130px" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_7.jpg" height="130px" width="130px" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-3">
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <img src="imgs/user_8.jpg" alt="" class="rounded-circle mb-3">

                      <h3 class="card-title mb-3">John Doe</h3>
                      <p class="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam alias eius maxime et at suscipit?
                      </p>
                      <a href="" class="mx-1"><img src="imgs/twi_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/fb_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/linkedin_icon.png" height="20px" width="20px" alt=""></a>
                      <a href="" class="mx-1"><img src="imgs/ig_icon.jpg" height="20px" width="20px" alt=""></a>
                    </div>
                  </div>
                </div>
              </div>

          </div>
        </div>