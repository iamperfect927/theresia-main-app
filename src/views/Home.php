<?php include 'temp/header.php' ?>
              
        <div class="main-body">
            <div class="body-head">
                <div class="overlay">
                    <h1>We are</h1>
                    <p>Bringing great minds together!</p>
                    
                </div>
                
                
            </div>


            <div class="container">
              <div class="mb-5 mt-4">
                <div class="team-bottom">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati voluptatum provident repellendus deleniti numquam qui eos beatae odio nulla laudantium, quod consequuntur placeat corporis, voluptas aut similique aperiam est possimus assumenda. Obcaecati explicabo quam alias repellat laudantium voluptatem necessitatibus similique dolorum minima cupiditate quasi delectus, id ullam molestiae quia eum possimus natus. Ea dolorem tempore quibusdam doloremque? Non corporis sit nesciunt, quam eum odio officia illum facere officiis ipsum perspiciatis dolorem exercitationem delectus suscipit! Consequuntur totam necessitatibus modi. Consequuntur, magnam.</p>
                </div>
                <div class="text-center">
                  <a href="JoinUsNow.html"><button type="submit" class="btn btn-primary">Join Us Now!</button></a>
                </div>
              </div>
                
            </div>

            
            <?php include "temp/team.php" ?>
              
            

        <?php include "temp/footer.php" ?>
    </div>

    <script src="dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>