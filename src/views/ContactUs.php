<?php include  "temp/header.php" ?>
               
        
              <div class="contact-main row">
                <div class="contact-sub1 row">
                  <h1>Contact Us</h1>
                </div>
              </div>

                <div class="container mt-5 mb-5">
                  <div class="row">

                    <div class="contact col">
                      <div class="text-start">
                        <h3>Contact</h3>
                      </div>
                      <div class="groove1 text-center pt-2">
                        <div class="team-sub2-1">
                          <a><img src="imgs/house-fill.svg" height="30px" width="30px" alt=""></a>
                          <h3>Address</h3>
                          <p>Ntaringkon Building, last floor.<br>Mile 18, Buea.<br>Cameroon.</p>
                        </div><br>

                        <div class="team-sub2-1">
                          <a><img src="imgs/telephone-fill.svg" height="30px" width="30px" alt=""></a>
                          <h3>Phone</h3>
                          <p>Tel: (+237) 652-768-274<br>Tel: (+237) 673-211-500<br>Tel: (+237) 657-447-445</p>
                        </div><br>

                        <div class="team-sub2-1">
                          <a><img src="imgs/mailbox2.svg" height="30px" width="30px" alt=""></a>
                          <h3>Email</h3>
                          <p>Email: djielaparfait99@gmail.com<br>Email: fomoafrica21@gmail.com</p>
                        </div>
                      </div>
                    </div>


                    <div class="left-margin col">
                      <form>
                        <div class="text-end">
                          <h3>Message Us</h3>
                        </div>
                        <div class="groove pt-4 ml-5">
                          <div class="mb-3">
                            <label class="form-label">Full name</label>
                            <input type="text" class="form-control" id="fullname1">
                            <div id="nameHelp" class="form-text">Please enter your name.</div>
                          </div>
                          <div class="mb-3">
                            <label class="form-label">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                          </div>
                          <div class="mb-3">
                            <label class="form-label">Message</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
                            <div id="textareaHelp" class="form-text">500 characters left.</div>
                          </div>
                          
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>
                    </div>
                  </div> 
                </div>

                
                
              
              
            

                <?php include  "temp/footer.php" ?>
    </div>

    <script src="dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>