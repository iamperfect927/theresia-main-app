<?php include "temp/header.php" ?>
               
              
              <div class="services-main row">
                  <div class="services-sub1 row">
                    <h1>Our Services</h1>
                  </div>
                

                  <section class="p-5">
                    <div class="container">
                          <div class="row text-center g-4">
                              <div class="col-md">
                                <div class="card bg-dark text-light">
                                  <div class="card-body text-center">
                                    <div class="h1 mb-3">
                                      <img src="imgs/branding.png" alt="Branding">
                                    </div>
                                    <h3 class="card-title mb-3">Branding</h3>
                                  </div>
                                </div>  
                              </div>
                              
                              <div class="col-md">
                                <div class="card bg-secondary text-light">
                                  <div class="card-body text-center">
                                    <div class="h1 mb-3">
                                      <img src="imgs/creativity.png" alt="Quality">
                                    </div>
                                    <h3 class="card-title mb-3">Quality</h3>
                                  </div>
                                </div>  
                              </div>

                              <div class="col-md">
                                <div class="card bg-dark text-light">
                                  <div class="card-body text-center">
                                    <div class="h1 mb-3">
                                      <img src="imgs/design.png" alt="Design">
                                    </div>
                                    <h3 class="card-title mb-3">Design</h3>
                                  </div>
                                </div>  
                              </div>

                              <div class="col-md">
                                <div class="card bg-secondary text-light">
                                  <div class="card-body text-center">
                                    <div class="h1 mb-3">
                                      <img src="imgs/creativity.png" alt="Creativity">
                                    </div>
                                    <h3 class="card-title mb-3">Creativity</h3>
                                  </div>
                                </div>  
                              </div>
                          </div>
                    </div>
                  </section>


                  <div class="section p-5">
                    <div class="container">
                      <div class="row text-center">
                        <div class="col-md">
                          <h1>Branding</h1>
                          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto repellendus voluptates animi excepturi reiciendis minus dolorum ex eligendi aliquam possimus.</p>
                        </div>
                        <div class="col-md">
                          <h1>Quality</h1>
                          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto repellendus voluptates animi excepturi reiciendis minus dolorum ex eligendi aliquam possimus.</p>
                        </div>
                      </div>

                      <div class="row text-center">
                        <div class="col-md">
                          <h1>Design</h1>
                          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto repellendus voluptates animi excepturi reiciendis minus dolorum ex eligendi aliquam possimus.</p>
                        </div>
                        <div class="col-md">
                          <h1>Creativity</h1>
                          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto repellendus voluptates animi excepturi reiciendis minus dolorum ex eligendi aliquam possimus.</p>
                        </div>
                      </div>
                    </div>
                  </div>

                
              </div>

              
            

              <?php include "temp/footer.php" ?>
    </div>

    <script src="dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>