
<?php include  "temp/header.php" ?>
              
              
              <div class="about">
                <div class="about-sub1">
                  <h1>About Us</h1>
                </div>

                <div class="container">
                  <div class="about-sub2 row text-center px-5 my-5">
                    <h1>Our Story</h1><br>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur sint atque maxime. 
                      Fugit harum corrupti, debitis similique perferendis consectetur, voluptatem minima enim pariatur,
                      totam beatae repellat! Dolorem est quos ea.
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur sint atque maxime. 
                      Fugit harum corrupti, debitis similique perferendis consectetur, voluptatem minima enim pariatur,
                        totam beatae repellat! Dolorem est quos ea.</p>
                  </div>
                  <div class="about-sub3 row text-center">
                    <h1>Our Goal</h1><br>
                    <h3>"Lorem ipsum dolor sit amet consectetur, adipisicing elit."</h3>
                    <br>
                    <div class="about-sub3-1 row mb-4 mt-3">
                      <div class="about-main1 col text-start">
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur sint atque maxime. 
                          Fugit harum corrupti, debitis similique perferendis consectetur, voluptatem minima enim pariatur,
                          totam beatae repellat! Dolorem est quos ea.</p>
                      </div>
                      
                      <div class="about-main2 col text-end">
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur sint atque maxime. 
                          Fugit harum corrupti, debitis similique perferendis consectetur, voluptatem minima enim pariatur,
                          totam beatae repellat! Dolorem est quos ea.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        

              
            

              <?php include  "temp/footer.php" ?>


    </div>

    <script src="dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>